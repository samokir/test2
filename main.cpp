#include <vector>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

int main(int argc, char** argv)
{
    srand(time(0));
    std::vector<char> buf;
    buf.reserve(atol(argv[2]));
    memcpy(buf.data(), argv[1], strlen(argv[1]));
    printf("password: %s %d\n", buf.data(), rand());

    memcpy(buf.data(), argv[2], strlen(argv[2]));
    printf("password: %s %d\n", buf.data(), rand());

    return 0;
}